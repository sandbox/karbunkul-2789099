<?php

namespace Drupal\ra;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for RaDatatype plugins.
 *
 * @package Drupal\ra
 */
abstract class RaDatatypeBase extends PluginBase implements RaDatatypeInterface {

  /**
   * Get Datatype label.
   *
   * @return string
   *   Datatype label.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get DataType description.
   *
   * @return string
   *   Datatype description.
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Get DataType type.
   *
   * @return string
   *   Datatype type.
   */
  public function getType() {
    return $this->pluginDefinition['id'];
  }

}
