<?php

namespace Drupal\ra;

use Drupal\Component\Plugin\PluginBase;
use Drupal\user\Entity\User;
use Emerap\Ra\RaConfig;

/**
 * Base class for RaDefinition method plugins.
 */
abstract class RaDefinitionBase extends PluginBase implements RaDefinitionInterface {

  /**
   * Get Definition description.
   *
   * @return string
   *   Definition description.
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Get Definition method name.
   *
   * @return string
   *   Definition method name.
   */
  public function getMethodName() {
    return $this->pluginDefinition['id'];
  }

  /**
   * Get Definition method params.
   *
   * @return string
   *   Definition method params.
   */
  public function getMethodParams() {
    return array();
  }

  /**
   * Get Definition section.
   *
   * @return string
   *   Definition section.
   */
  public function getSection() {
    return 'Custom';
  }

  /**
   * Get Definition public access.
   *
   * @return string
   *   Definition access state.
   */
  public function isPublic() {
    return TRUE;
  }

  /**
   * Get Definition access callback.
   *
   * @return string
   *   Definition access callback.
   */
  public function getAccessCallback() {
    $user = User::load(RaConfig::getUserId());
    return [$user, 'hasPermission'];
  }

  /**
   * Get Definition access params.
   *
   * @return string
   *   Definition access params for access callback.
   */
  public function getAccessParams() {
    return 'access content';
  }

}
