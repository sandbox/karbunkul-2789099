<?php

namespace Drupal\ra\Command;

use Drupal\Console\Generator\Generator;

/**
 * Class RaFormatGenerator.
 *
 * @package Drupal\ra\Command
 */
class RaFormatGenerator extends Generator {

  /**
   * Generate RaFormat.
   *
   * @param string $module
   *   Module name.
   * @param string $id
   *   Format id.
   * @param string $description
   *   Format description.
   * @param string $label
   *   Format label.
   * @param string $mime_type
   *   Format mime-type.
   */
  public function generate($module, $id, $description, $label, $mime_type) {
    $template = 'generate-ra-format.html.twig';

    $class = ucfirst($id) . 'Format';

    $parameters = [
      'module' => $module,
      'class' => $class,
      'id' => $id,
      'description' => $description,
      'label' => $label,
      'mime_type' => $mime_type,
    ];

    $target = $this->getSite()
        ->getModulePath($module) . '/src/Plugin/RaFormat/' . $class . '.php';

    $this->renderFile(
      $template,
      $target,
      $parameters
    );
  }

}
