<?php

namespace Drupal\ra\Command;

use Drupal\Console\Generator\Generator;

/**
 * Class RaDefinitionGenerator.
 *
 * @package Drupal\ra\Command
 */
class RaDefinitionGenerator extends Generator {

  /**
   * Helper function - id to class name.
   *
   * @param string $id
   *   Option id.
   *
   * @return string
   *   Generated class name string.
   */
  static public function idToClass($id) {
    return str_replace('.', '', ucwords($id, '.')) . 'Definition';
  }

  /**
   * Helper function - id to definition name.
   *
   * @param string $id
   *   Option id.
   *
   * @return string
   *   Generated class name string.
   */
  private function idToDefinitionId($id) {
    $id = ucwords($id, '.');
    $words = explode('.', $id);
    $definition_id = strtolower($words[0]) . '.' . strtolower($words[1]);
    return $definition_id . implode('', array_slice($words, 2));
  }

  /**
   * Generate RaDefinition.
   *
   * @param string $module
   *   Module name.
   * @param string $id
   *   Definition id.
   * @param string $description
   *   Definition description.
   */
  public function generate($module, $id, $description) {
    $template = 'generate-ra-definition.html.twig';

    $class = self::idToClass($id);

    $parameters = [
      'module' => $module,
      'class' => $class,
      'id' => $this->idToDefinitionId($id),
      'description' => $description,
    ];

    $target = $this->getSite()
        ->getModulePath($module) . '/src/Plugin/RaDefinition/' . $class . '.php';

    $this->renderFile(
      $template,
      $target,
      $parameters
    );
  }

}
