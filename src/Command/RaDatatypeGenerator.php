<?php

namespace Drupal\ra\Command;

use Drupal\Console\Generator\Generator;

/**
 * Class RaDatatypeGenerator.
 *
 * @package Drupal\ra\Command
 */
class RaDatatypeGenerator extends Generator {

  /**
   * Generate RaDatatype.
   *
   * @param string $module
   *   Module name.
   * @param string $id
   *   Datatype id.
   * @param string $description
   *   Datatype description.
   * @param string $label
   *   Datatype label.
   */
  public function generate($module, $id, $description, $label) {
    $template = 'generate-ra-datatype.html.twig';

    $class = ucfirst($id) . 'Type';

    $parameters = [
      'module' => $module,
      'class' => $class,
      'id' => $id,
      'description' => $description,
      'label' => $label,
    ];

    $target = $this->getSite()
        ->getModulePath($module) . '/src/Plugin/RaDatatype/' . $class . '.php';

    $this->renderFile(
      $template,
      $target,
      $parameters
    );
  }

}
