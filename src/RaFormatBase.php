<?php

namespace Drupal\ra;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for RaFormat plugins.
 */
abstract class RaFormatBase extends PluginBase implements RaFormatInterface {

  /**
   * Get Format label.
   *
   * @return string
   *   Format label.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get Format type.
   *
   * @return string
   *   Format type.
   */
  public function getType() {
    return $this->pluginDefinition['id'];
  }

  /**
   * Get Format description.
   *
   * @return string
   *   Format description.
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Get Format mime-type.
   *
   * @return string
   *   Format mime-type.
   */
  public function getMimeType() {
    return $this->pluginDefinition['mime_type'];
  }

  /**
   * Check requirements.
   *
   * @return bool
   *   Requirements state.
   */
  public function requirements() {
    return TRUE;
  }

}
