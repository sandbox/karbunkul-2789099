<?php

namespace Drupal\ra\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RaController.
 *
 * @package Drupal\ra\Controller
 */
class RaController extends ControllerBase {

  /**
   * Rapid API framework api callback.
   *
   * @param string $method
   *   Method name.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\Response Result rapid api method callback.
   *   Result rapid api method callback.
   */
  public function method($method, Request $request) {

    $args = ($request->getMethod() == 'GET') ? $_GET : $_POST;

    $ra = ra_config()->instanceRa();
    $methodObj = ra_config()->instanceMethod($method, $args);

    $result = $ra->call($methodObj);
    $format = $result->getFormat();

    $response = new Response();
    $content = [
      '#type' => 'inline_template',
      '#template' => '{% autoescape false %}{{ request }}{% endautoescape %}',
      '#context' => [
        'request' => $result->format(),
      ],
    ];
    $response->setContent(render($content));
    $response->headers->set('Content-Type', $format->getMimeType() . ' charset=UTF-8');

    return $response;
  }

}
