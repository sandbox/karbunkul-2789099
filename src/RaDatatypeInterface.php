<?php

namespace Drupal\ra;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Emerap\Ra\Core\DatatypeInterface;

/**
 * Defines an interface for RaDatatype plugins.
 */
interface RaDatatypeInterface extends DatatypeInterface, PluginInspectionInterface {

}
