<?php

namespace Drupal\ra_docs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RaDocsForm.
 *
 * @package Drupal\ra_docs\Form
 */
class RaDocsForm extends FormBase {

  /**
   * Method name.
   *
   * @var string $methodName
   */
  protected $methodName;

  /**
   * Method definition instance.
   *
   * @var \Emerap\Ra\Core\Definition $methodDefinition
   */
  protected $methodDefinition;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $this->methodName = $this->getRouteMatch()->getParameter('method');
    $this->methodDefinition = ra_config()->instanceDefinition()
      ->getDefinitionByName($this->methodName);
    return 'ra_docs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#prefix'] = '<div id="ra-dev-method-form-wrapper">';
    $form['#suffix'] = '</div>';

    if (!is_null($this->methodName)) {

      $definition = $this->methodDefinition;
      $token_warning = $this->t('This method can be called with a token.');
      if ($definition->isSecurity()) {
        $form['warning'] = [
          '#type' => 'markup',
          '#markup' => '<div class="messages messages--warning">' . $token_warning . '</div>',
          '#weight' => -50,
        ];
      }

      foreach ($definition->getMethodParams() as $param) {
        if ($param->getName() !== 'token') {
          $optionsCount = count($param->getOptions());
          $isOptions = ($optionsCount > 1) ? TRUE : FALSE;

          $optionsKind = 'radios';

          if ($optionsCount > 2) {
            $optionsKind = 'select';
          }

          $form['param_' . $param->getName()] = [
            '#title' => 'Parameter - ' . $param->getName(),
            '#type' => ($isOptions) ? $optionsKind : 'textfield',
            '#required' => $param->isRequire(),
            '#description' => ((!empty($param->getHelp())) ? $param->getHelp() : $param->getTypeObject()->getDescription()) . ' (' . $param->getTypeObject()->getLabel() . ')',
            '#default_value' => $param->getDefault(),
          ];

          if ($isOptions) {
            $form['param_' . $param->getName()]['#options'] = $param->getOptions();
          }
        }
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => -20,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Call method'),
      '#button_type' => 'primary',
      '#ajax' => [
        'wrapper' => 'ra-dev-method-form-wrapper',
      ],
    ];

    if ($form_state->isSubmitted()) {

      $params = [];
      foreach ($this->methodDefinition->getMethodParams() as $parameter) {
        $value = $form_state->getValue('param_' . $parameter->getName());
        $params[$parameter->getName()] = (!empty($value)) ? $value : $parameter->getDefault();
      }

      $time_before = microtime(TRUE);

      $client_id = NULL;
      if ($this->methodDefinition->isSecurity() && ($client = $this->registerClient())) {
        $params['token'] = $client->getToken();
        $client_id = $client->getClientId();
      }

      $result = $this->call($params);

      if (($result->getError()->getCode() === 201) || ($result->getError()->getCode() === 105)) {
        if ($token = ra_config()->instanceToken()->updateToken($client_id)) {
          $params['token'] = $token;
          drupal_set_message($this->t('Token was updated'), 'warning');
          $result = $this->call($params);
        }
      }

      $total_result = $result->format();

      $time_after = microtime(TRUE);
      $time_request = round(($time_after - $time_before) * 1000, 0);
      drupal_set_message($this->t('Time request: %time ms',
        ['%time' => $time_request]));
      drupal_set_message($this->t('Request length: %length bytes',
        ['%length' => mb_strlen($total_result)]));

      $message_type = ($result->getError()->getCode() > 0) ? 'error' : 'status';

      $form['result'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="messages messages--{{ type }}">{{ request }}</div>',
        '#context' => [
          'request' => $result->format(),
          'type' => $message_type,
        ],
        '#weight' => -10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Register client.
   *
   * @return \Emerap\Ra\Core\ServerClient
   *   ServerClient instance.
   */
  private function registerClient() {
    $client = ra_config()->instanceServerClient()
      ->getClientByTag('ra_docs', ra_config()->getUserID());

    if (!$client) {
      $invite = ra_config()->instanceServerClient()
        ->pair('ra_docs', rand(1000, 9999), 'Drupal 8');
      ra_config()->instanceServerClient()
        ->activate($invite['invite_id'], $invite['pin'], ra_config()->getUserID());
    }

    return ra_config()->instanceServerClient()
      ->getClientByTag('ra_docs', ra_config()->getUserID());
  }

  /**
   * Helper for call method.
   *
   * @param array $params
   *   Method params.
   *
   * @return \Emerap\Ra\Core\Result
   *   Method result instance.
   */
  private function call($params = []) {
    $ra = ra_config()->instanceRa();
    $method = ra_config()->instanceMethod($this->methodName, $params);
    return $ra->call($method);
  }

}
