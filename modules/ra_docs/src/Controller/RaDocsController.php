<?php

namespace Drupal\ra_docs\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class RaDocsController.
 *
 * @package Drupal\ra_docs\Controller
 */
class RaDocsController extends ControllerBase {

  /**
   * Docs main page.
   *
   * @return string
   *   Return Hello string.
   */
  public function docsMain() {
    $build = [];

    if (ra_config()) {
      $build['available_methods'] = [
        '#type' => 'theme',
        '#theme' => 'ra_docs_section',
        '#label' => 'Methods',
        '#items' => $this->getAvailableMethods(),
      ];

      $build['available_data-types'] = [
        '#type' => 'theme',
        '#theme' => 'ra_docs_section',
        '#label' => 'DataTypes',
        '#items' => $this->getAvailableDatatypes(),
      ];

      $build['available_formats'] = [
        '#type' => 'theme',
        '#theme' => 'ra_docs_section',
        '#label' => 'Formats',
        '#items' => $this->getAvailableFormats(),
      ];
    }

    $build['#attached']['library'][] = 'ra_docs/methods_list';
    return $build;
  }

  /**
   * Docs method page callback.
   *
   * @param string $method
   *   Method name.
   *
   * @return array
   *   Method content.
   */
  public function docsMethod($method) {
    return [
      $this->formBuilder()->getForm('Drupal\ra_docs\Form\RaDocsForm'),
    ];
  }

  /**
   * Docs method title callback.
   *
   * @param string $method
   *   Method name.
   *
   * @return string
   *   Page method title.
   */
  public function docsMethodTitle($method) {
    return 'Method - ' . $method;
  }

  /**
   * Get available formats.
   *
   * @return array
   *   All available formats.
   */
  private function getAvailableFormats() {
    $items = [];
    foreach (ra_config()->getFormats() as $format) {
      /** @var \Emerap\Ra\Core\FormatInterface $format */
      $items[] = [
        'id' => $format->getType(),
        'title' => $format->getLabel(),
        'description' => $format->getDescription(),
      ];
    }

    return $items;
  }

  /**
   * Get available methods.
   *
   * @return array
   *   All available methods.
   */
  private function getAvailableMethods() {
    $items = [];
    foreach (ra_config()->getDefinitions() as $definition) {
      /** @var \Emerap\Ra\Core\Definition $definition */
      $items[] = [
        'id' => $definition->getName(),
        'title' => $definition->getName(),
        'description' => $definition->getDescription(),
        'url' => Url::fromRoute('ra_docs.method', ['method' => $definition->getName()]),
      ];

    }

    return $items;
  }

  /**
   * Get available datatypes.
   *
   * @return array
   *   All available datatypes.
   */
  private function getAvailableDatatypes() {
    $items = [];
    foreach (ra_config()->getDatatypes() as $datatype) {
      /** @var \Emerap\Ra\Core\DatatypeInterface $datatype */
      $items[] = [
        'id' => $datatype->getType(),
        'title' => $datatype->getLabel(),
        'description' => $datatype->getDescription(),
      ];
    }

    return $items;
  }

}
