# Rapid API for Drupal 8

## Install

- [Drush](http://www.drush.org/en/master/install) install: `drush dl ra && drush en ra ra_docs -y`
- Install composer dependencies: `composer install`

## Drupal console commands

- Generate plugin RaDatatype - `drupal generate:plugin:ra:datatype`
- Generate plugin RaDefinition - `drupal generate:plugin:ra:definition`
- Generate plugin RaFormat - `drupal generate:plugin:ra:format`

## Basic usage

```
// Method name
$method_name = 'ra.version';

// Method params
$params = ['field' => 'engine'];

// \Emerap\Ra\Core\Method instance
$method = ra_config()->instanceMethod($method_name, $params);

// Call api method
$result = ra_config()->instanceRa->call();

// Get method response
$response = $result->format();
```

## Links

- Rapid API core: [source](https://github.com/emerap/ra) | [packagist](https://packagist.org/packages/emerap/ra)
- Community page: [vk.com/rapid_api](https://vk.com/rapid_api)

Copyright &copy; 2016 [ [Pokhodyun Alexander](https://vk.com/karbunkul)]